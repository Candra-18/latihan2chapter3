// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi,
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';

// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'

// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil))

// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/

// 1
class Vehicle {
  constructor(jenis, produksi) {
    this.jenis = jenis;
    this.produksi = produksi;
  }
  info() {
    console.log(`Jenis Kendaraan roda ${this.jenis} dari negara ${this.produksi} `);
  }
}

// 2
class Mobil extends Vehicle {
  constructor(jenis, produksi, harga, merek, pajak) {
    super(jenis, produksi);
    this.harga = harga;
    this.merek = merek;
    this.pajak = pajak;
  }
  //   3
  totalprice() {
    return this.harga + (this.pajak / 100) * this.harga;
  }
  panggilinfo() {
    super.info();
    this.totalprice();
    console.log(`Kendaraan ini nama mereknya ${this.merek} dengan total harga Rp. ${this.totalprice()}.000.000`);
  }
}

// 5
const kendaraan = new Vehicle(2, "Jepang");
kendaraan.info();
const mobil = new Mobil(4, "Jerman", 800, "Mercedes", 10);
mobil.panggilinfo();
